from setuptools import setup
 
setup(
    name='cmdb_models',    # This is the name of your PyPI-package.
    version='0.2',
    install_requires=['peewee', 'psycopg2'],
    packages=['cmdb_models']
)
