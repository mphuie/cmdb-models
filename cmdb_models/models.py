from peewee import *
from playhouse.postgres_ext import *
from playhouse.db_url import connect
import os
import json

database = connect(os.environ['DATABASE_URL'], register_hstore=False)

class BaseModel(Model):
  class Meta:
    database = database

class Environment(BaseModel):
  name = CharField()
  description = CharField(null=True)
  contact = CharField(null=True)
  active = BooleanField(default=True)
  creation_date = DateField(null=True)

  def to_dict(self):
    r = {}
    for k in self._data.keys():
      r[k] = getattr(self, k)
    return r

class Host(BaseModel):
  name = CharField()
  description = CharField(null=True)
  metadata = JSONField(default={})
  sources = JSONField(default=[])
  creation_date = DateField(null=True)
  contact = CharField(null=True)
  active = BooleanField(default=True)
  type = CharField(null=True)
  domain = CharField()
  datacenter = CharField(null=True)
  environment = ForeignKeyField(Environment, null=True, related_name='hosts')

class Ip(BaseModel):
  ip = CharField()
  host = ForeignKeyField(Host, null=True, related_name='ips')
  active = BooleanField(null=True)
  open_ports = JSONField(default=[])

class AdComputer(BaseModel):
  name = CharField()
  host = ForeignKeyField(Host, null=True, related_name='adentry')
  active = BooleanField(null=True)

class DnsEntry(BaseModel):
  hostname = CharField()
  record_type = CharField()
  record_data = CharField()
  timestamp = DateTimeField(null=True)
  active = BooleanField(null=True)

  def to_dict(self):
    r = {}
    for k in self._data.keys():
      r[k] = getattr(self, k)
    return r

class Trash(BaseModel):
  source = CharField()
  name = CharField(null=True)
  ip = CharField(null=True)
  reason = CharField(null=True)
  metadata = JSONField(default={})
  is_deleted = BooleanField(default=False)

  def to_dict(self):
    r = {}
    for k in self._data.keys():
      r[k] = getattr(self, k)
    return r

class Team(BaseModel):
  name = CharField()
  email_dl = CharField(null=True)

class Member(BaseModel):
  username = CharField()
  is_approver = BooleanField(default=False)
  team = ForeignKeyField(Team, null=True, related_name='members')

class Team(BaseModel):
  name = CharField()
  email_dl = CharField(null=True)
  approver = ForeignKeyField(Member, null=True, related_name='managed') 

class Member(BaseModel):
  username = CharField()
  is_approver = BooleanField(default=False)
  team = ForeignKeyField(Team, null=True, related_name='members')

class Hardware(BaseModel):
  manufacturer = CharField()
  model = CharField()
  type = CharField(null=True)